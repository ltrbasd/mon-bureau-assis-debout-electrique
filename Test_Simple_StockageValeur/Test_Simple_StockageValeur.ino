/**
 * Exemple de code de lecture et de manipulation de la NVRAM d'un module RTC DS1307.
 * Incrémente une nouvelle valeur de stockage a chaque reboot de l'arduino
 * Compatible Arduino 0023 et Arduino 1.x (et supérieur).
 * 
 * Fourni/adapté par le site Carnet du Maker
 */

/* Dépendances */
#include <Wire.h>
#include "DS1307.h"


/* Rétro-compatibilité avec Arduino 1.x et antérieur */
#if ARDUINO >= 100
#define Wire_write(x) Wire.write(x)
#define Wire_read() Wire.read()
#else
#define Wire_write(x) Wire.send(x)
#define Wire_read() Wire.receive()
#endif


/** Fonction de lecture de la mémoire non volatile du module RTC (56 octets maximum) */
int read_nvram_memory(byte address) {
  
  /* Ne lit pas en dehors de la NVRAM */
  if (address > DS1307_NVRAM_SIZE)
    return -1;
  
  /* Début de la transaction I2C */
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire_write(DS1307_NVRAM_BASE + address); // Lecture mémoire NVRAM
  Wire.endTransmission(); // Fin de la transaction I2C
 
  /* Lit un octet depuis la mémoire du module RTC */
  Wire.requestFrom(DS1307_ADDRESS, (byte) 1);
  return Wire_read();
}


/** Fonction d'écriture de la mémoire non volatile du module RTC (56 octets maximum) */
void write_nvram_memory(byte address, byte data) {
  
  /* N'écrit pas en dehors de la NVRAM */
  if (address > DS1307_NVRAM_SIZE)
    return;
  
  /* Début de la transaction I2C */
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire_write(DS1307_NVRAM_BASE + address); // Ecriture mémoire NVRAM
  Wire_write(data);
  Wire.endTransmission(); // Fin de transaction I2C
}


/** Fonction setup() */
void setup() {
  
  /* Initialise le port I2C */
  Serial.begin(9600);
  
  /* Initialise le port I2C */
  Wire.begin();
  
  /* Lit la valeur actuelle */
  byte value = read_nvram_memory(0);
  Serial.print(F("Valeur actuelle : "));
  Serial.println(value);
  
  /* Met à jour la valeur */
  Serial.print(F("Nouvelle valeur : "));
  Serial.println(value + 3);
  write_nvram_memory(0, value + 3);
}


/** Fonction loop() */
void loop() {
  // Rien à faire
}
