//
// Bureau Assis-Debout Electrique
// Programme de test du bon fonctionnement de la liason bluetooth
// (adapté de multiples exemples trouvés sur internet)
// Après recherche, il existe de multiples applis pédagogiques déja toutes écrites (rechercher "bluetooth controler led" sur google play")
//
// Fonctionne avec n'importe quel terminal bluetooth installé sur votre portable.
// Perso, j'ai utilisé un terminal très léger pour faire mes tests: "Bluetooth Terminal" de Juan Sebastian Ochoa Zambrano, 229ko
//
// Usage :
// - Si la commande "ON" est reçue sur l'arduino depuis le portable, la LED du port 13 de l'arduino s'allume
// - Si la commande "OFF" est reçue sur l'arduino depuis le portable, la LED du port 13 de l'arduino s'éteint
// - Toute autre commande est ignorée
//
// Juillet-Aout-Septembre 2021
// Licence : CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//
#include <SoftwareSerial.h>
SoftwareSerial hc06(2, 3);
String cmd = "";
float sensor_val = 0;
void setup() {
  //Initialize Serial Monitor
  Serial.begin(9600);
  //Initialize Bluetooth Serial Port
  hc06.begin(9600);
  pinMode(13, OUTPUT); //L1 est une broche de sortie
  digitalWrite(13, LOW); // Allumer LED
  Serial.println("dEBUT bUETOOTH !");
}
void loop() {
  //Read data from HC06
  while (hc06.available() > 0) {
    cmd += (char)hc06.read();
  }
  //Select function with cmd
  if (cmd != "") {
    Serial.print("Command recieved : ");
    Serial.println(cmd);
    // We expect ON or OFF from bluetooth
    if (cmd == "ON") {
      Serial.println("LED 13 is on");
      digitalWrite(13, HIGH); // Allumer LED

    } else if (cmd == "OFF") {
      Serial.println("LED 13 is off");
      digitalWrite(13, LOW); // Allumer LED

    } else {
      Serial.println("LED is off by default");
    }
    cmd = ""; //reset cmd
  }
  delay(100);
}
