// Bureau assis debout individuel électrique à vérins
//
// Programme de test de relevé moyenné des retours du capteur à ultrason
//
// Adapté de Arduino Moving Average Library
// https://github.com/JChristensen/movingAvg
// Copyright (C) 2018 by Jack Christensen and licensed under
// GNU GPL v3.0, https://www.gnu.org/licenses/gpl.html
//

#include <movingAvg.h>                  // https://github.com/JChristensen/movingAvg


/* Constantes pour les broches */
const byte TRIGGER_PIN = 4; // Broche TRIGGER
const byte ECHO_PIN = 5;    // Broche ECHO


/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 15000UL; // 25ms = ~8m à 340m/s

/* Vitesse du son dans l'air en mm/us */
const float SOUND_SPEED = 344.0 / 1000;

movingAvg avgTemp(8);                  // define the moving average object

void setup()
{
  Serial.begin(115200);
  avgTemp.begin();                            // initialize the moving average

  /* Initialise les broches */
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(ECHO_PIN, INPUT);

  avgTemp.reading(1); // Remplir le tableau de mesure avec des valeurs
  avgTemp.reading(2);
  avgTemp.reading(3);
  avgTemp.reading(4);
  avgTemp.reading(5);
  avgTemp.reading(6);
  avgTemp.reading(7);
  avgTemp.reading(8);

}

void loop()
{
  /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);

  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);

  // 3. Calcul la distance à partir du temps mesuré
  float distance_mm = measure / 2.0 * SOUND_SPEED;

  float avg = avgTemp.reading(distance_mm);             // Lit une nouvelle valeur de distance et renvoi la moyenne

  /* Affiche les résultats en mm, cm et m */
  //Serial.print(F("Distance: "));
  Serial.print(distance_mm);
  Serial.print(F("mm, "));
  //Serial.print(distance_mm / 10.0, 2);
  //Serial.print(F("cm, "));
  //Serial.print(distance_mm / 1000.0, 2);
  //Serial.println(F("m)"));
  Serial.print(F("Distance Moyenne 8: "));
  Serial.println(avg);

  delay(250);
}
