// Bureau assis debout individuel électrique à vérins
//
// Programme de test de relevé moyenné des retours du capteur à ultrason
//
// Adapté de Arduino Moving Average Library
// https://github.com/JChristensen/movingAvg
// Copyright (C) 2018 by Jack Christensen and licensed under
// GNU GPL v3.0, https://www.gnu.org/licenses/gpl.html
//

#include <movingAvg.h>                  // https://github.com/JChristensen/movingAvg


/* Constantes pour les broches */
const byte TRIGGER_PIN1 = 2; // Broche TRIGGER
const byte ECHO_PIN1 = 3;    // Broche ECHO

const byte TRIGGER_PIN2 = 4; // Broche TRIGGER
const byte ECHO_PIN2 = 5;    // Broche ECHO


/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 15000UL; // 25ms = ~8m à 340m/s

/* Vitesse du son dans l'air en mm/us */
const float SOUND_SPEED = 344.0 / 1000;

movingAvg avgTemp1(8);                  // define the moving average object
movingAvg avgTemp2(8);                  // define the moving average object

void setup()
{
  Serial.begin(115200);
  avgTemp1.begin();                            // initialize the moving average
  avgTemp2.begin();                            // initialize the moving average

  /* Initialise les broches */
  pinMode(TRIGGER_PIN1, OUTPUT);
  digitalWrite(TRIGGER_PIN1, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(ECHO_PIN1, INPUT);
 pinMode(TRIGGER_PIN2, OUTPUT);
  digitalWrite(TRIGGER_PIN2, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(ECHO_PIN2, INPUT);

  avgTemp1.reading(1); // Remplir le tableau de mesure avec des valeurs
  avgTemp1.reading(2);
  avgTemp1.reading(3);
  avgTemp1.reading(4);
  avgTemp1.reading(5);
  avgTemp1.reading(6);
  avgTemp1.reading(7);
  avgTemp1.reading(8);

  avgTemp2.reading(1); // Remplir le tableau de mesure avec des valeurs
  avgTemp2.reading(2);
  avgTemp2.reading(3);
  avgTemp2.reading(4);
  avgTemp2.reading(5);
  avgTemp2.reading(6);
  avgTemp2.reading(7);
  avgTemp2.reading(8);

}

void loop()
{
  /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
  digitalWrite(TRIGGER_PIN1, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN1, LOW);
  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure1 = pulseIn(ECHO_PIN1, HIGH, MEASURE_TIMEOUT);
  // 3. Calcul la distance à partir du temps mesuré
  float distance_mm_1 = measure1 / 2.0 * SOUND_SPEED;

  float avg1 = avgTemp1.reading(distance_mm_1);             // Lit une nouvelle valeur de distance et renvoi la moyenne

  /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
  digitalWrite(TRIGGER_PIN2, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN2, LOW);
  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure2 = pulseIn(ECHO_PIN2, HIGH, MEASURE_TIMEOUT);
  // 3. Calcul la distance à partir du temps mesuré
  float distance_mm_2 = measure2 / 2.0 * SOUND_SPEED;

  float avg2 = avgTemp2.reading(distance_mm_2);             // Lit une nouvelle valeur de distance et renvoi la moyenne


  /* Affiche les résultats en mm, cm et m */
  //Serial.print(F("Distance: "));
  Serial.print(distance_mm_2);
  Serial.print(F(" mm, "));
  Serial.print(distance_mm_1);
  Serial.print(F(" mm, "));
  //Serial.print(distance_mm / 10.0, 2);
  //Serial.print(F("cm, "));
  //Serial.print(distance_mm / 1000.0, 2);
  //Serial.println(F("m)"));
  Serial.print(F(" Distance Moyenne 8_G: "));
  Serial.print(avg2);
  Serial.print(F(" Distance Moyenne 8_D: "));
  Serial.println(avg1);

  delay(250);
}
