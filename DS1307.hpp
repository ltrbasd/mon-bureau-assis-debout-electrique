#ifndef ds1307_HPP
#define ds1307_HPP

// Fonctions de manipulation de la NVRAM du DS1307

int NVRAMWritelong(byte address, long value)
{
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(address + DS1307_NVRAM_BASE);

  Wire.write(value & 0xFF);
  value >>= 8;
  Wire.write(value & 0xFF);
  value >>= 8;
  Wire.write(value & 0xFF);
  value >>= 8;
  Wire.write(value & 0xFF);

  return Wire.endTransmission();
}

long NVRAMReadlong(int address) {
  byte neededBytes = 4;
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(address + DS1307_NVRAM_BASE);
  Wire.endTransmission();
  Wire.requestFrom( (uint8_t)DS1307_ADDRESS, neededBytes );

  //Read the 4 bytes from the nvram memory.
  long four = Wire.read();
  long three = Wire.read();
  long two = Wire.read();
  long one = Wire.read();

  //Return the recomposed long by using bitshift.
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}


#endif 
