#ifndef CommandeVerins_H
#define CommandeVerins_H

// Constantes pour les broches des vérins WPI409
//4 broches pour commander le sens des 2 moteurs
const byte BorcheSensV1a = 6;
const byte BorcheSensV1b = 7;
const byte BorcheSensV2a = 8;
const byte BorcheSensV2b = 9;
// 2 broches PWM pour définir la vitesse des moteurs/vérins
const byte BorcheVitesseV1 = 10;
const byte BorcheVitesseV2 = 11;

// Constante pour simplifier la lecture du programme (Sens de fonctionnement du vérin)
const boolean VERS_HAUT = 1;
const boolean VERS_BAS = 0;

// Valeur de la vitesse pour mise en route des vérins
byte vitesse = 255;
byte vitesseG = 255;
byte vitesseD = 255;

// Variables transitoires utilisées  lorsque le plateau va vers une postion pré réglée
boolean Var_VersPosH_enCours = 0;
boolean Var_VersPosB_enCours = 0;

#endif 
