//
// Bureau Assis-Debout Electrique
// Programme de commande arduino incluant toutes les fonctionnalités
//
// (Version 1, non optimisée, juste fonctionnellle)
//
// Avec boutons, vérins, capteur ultra-son
//
// Ajout de la fonction (F = Progmem) pour économiser de la place sur les chaines de caractères de debug [println( => println(F)]
//
//
// Usage :
// - Bouton stop : Arret du/des vérins + boucle pour mise à jour de la hauteur du plateau (ultra son) + réinitialisation des variables transitoires
// - Manuel haut ou bas et automatique haut ou bas : Mise en marche des 2 vérins dans le sens demandé jusqu'à arret manuel
// - Bouton Enregistrer position + automatique haut ou automatique bas : Enregistrer la hauteur actuelle du plateau
// - Automatique haut ou bas : Mise en marche des 2 vérins dans le sens demandé jusqu'à la hauteur enregistrée dans la mémoire du DS 1307
// - Enregistrement position + manuel haut ou bas : Mise en marche du seul vérin gauche pour horizontalisation manuelle du plateau
// - Enregistrer Position + Stop : Horizontalisation automatique (à l'aide de l'inclinomètre)
//
// Juillet-Aout-Septembre 2021
// Licence : CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//

// Bibilothèque pour avoir une moyenne glissante lors des mesures de distance (et d'inclinaison)
// Non indispensable (peut ne pas être utilisée [Attention aux incohérences]) ou être réalisée manuellement dans la fonction
#include <movingAvg.h>                  // https://github.com/JChristensen/movingAvg

// Bibliothèque utilisée pour la communication Bluetooth entre Le portable et l'Arduino
#include <SoftwareSerial.h>

// Fichier contenant l'affectation globale des broches de la carte ARDUINO
#include "AffectationBroches.h"

// Déclaration d'un objet SoftwareSerial = Bluetooth
SoftwareSerial hc06(BrocheBluetoothTX, BrocheBluetoothRX);

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le shield moteur + constantes et variables
#include "CommandeVerins.h"

// Fichier contenant les fonctions de commande des vérins
#include "Verins.hpp"


// Fichier contenant les constantes pour la gestion de la mémoire NVRAM
#include "DS1307.h"

// Fichier contenant les fonctions pour la gestion de la mémoire NVRAM
#include "DS1307.hpp"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le capteur ultra son + constantes et variables
#include "UltraSon.h"

// Fichier contenant les fonctions pour le capteur ultra son
#include "UltraSon.hpp"


// Fichier contenant les constantes pour la gestion de l'inclinomètre (VMA208)
#include "Horizontalisation.h"

// Fichier contenant les fonctions pour la gestion de l'inclinomètre (VMA208)
#include "Horizontalisation.hpp"

// Chaine recevant les octets du bluetooth
String cmdBT = "";

//
//
// Bloc de définition initial
void setup() {
  // Initialise le port série
  Serial.begin(9600);

  // Initialise le port I2C
  Wire.begin();

  // Initialise l'objet Bluetooth
  hc06.begin(9600);

  // Phrase de description du programme lors de son initialisation (Pour le debug sur le port série)
  Serial.println(F(""));
  Serial.println(F("Bureau Assis-Debout Electrique."));
  Serial.println(F("Debut programme de commande complet (Boutons+vérins+distance+nvram+horizontal)."));

  // Définition des broches de commande des vérins en Sortie
  pinMode(BorcheSensV1a, OUTPUT);
  pinMode(BorcheSensV1b, OUTPUT);
  pinMode(BorcheSensV2a, OUTPUT);
  pinMode(BorcheSensV2b, OUTPUT);

  // On met toutes les broches de commande des vérins à HIGH pour bien s'assurer de ne pas mettre en route les vérins à l'initialisation
  digitalWrite(BorcheSensV1a, HIGH);
  digitalWrite(BorcheSensV1b, HIGH);
  digitalWrite(BorcheSensV2a, HIGH);
  digitalWrite(BorcheSensV2b, HIGH);

  // Définition des broches de commande des boutons de commande en Entrée avec activation de la résistance interne de tirage au VCC.
  pinMode(BoutonStop, INPUT_PULLUP);
  pinMode(BoutonVersPosHaut, INPUT_PULLUP);
  pinMode(BoutonVersPosBas, INPUT_PULLUP);
  pinMode(BoutonManHaut, INPUT_PULLUP);
  pinMode(BoutonManBas, INPUT_PULLUP);
  pinMode(BoutonEnregPos, INPUT_PULLUP);

  //
  // Partie capteur ultra son
  //
  // initialise l'objet pour la mesure de distance moyenne
  DistM.begin();

  //  Initialise les broches pour le capteur ultrason
  pinMode(BROCHE_TRIGGER, OUTPUT);
  digitalWrite(BROCHE_TRIGGER, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(BROCHE_ECHO, INPUT);

  // Pré-Remplir le tableau de mesures par ultra son pour avoir immédiatement un résultat cohérent
  long distance_mm = Mesure_ultra_Son_instantanee ();

  // Mettre X fois la mesure instantanée dans le tableau
  for ( byte i = 1; i <= 16; i++) {
    DistM.reading (distance_mm);
    delay(50);
  }
  Serial.print(F("Distance initiale : "));
  Serial.println(DistM.getAvg());

  // Lecture de la valeur des positions de hauteur du plateau pré-réglées à l'initialisation
  // Prévoir quelque chose pour vérifier que la variable a bien été intialisée ? (Ne semble pas utile, les fins de courses des vérins devraient suffir))
  long ValEnrPosBasse = NVRAMReadlong(AddReglageBas);
  long ValEnrPosHaute = NVRAMReadlong(AddReglageHaut);
  //Serial.println(ValEnrPosBasse);
  //Serial.println(ValEnrPosHaute);

  //
  // Partie Capteur inclinaison
  //
  // Initialisation de l'objet inclinomètre
  inclin.init();

  // Initialisation de l'objet tableau de mesure de la moyenne des angles pour avoir immédiatement un résultat cohérent
  AngleMoy.begin();

  // Boucle initiale pour remplir le tableau  (et connaitre l'inclinaison initiale du plateau)
  for ( byte i = 1; i <= 16; i++) {
    LectureInclinometre ();
    delay(20);
  }
  //Serial.print(F("Angle moyen initial : "));
  //Serial.println(AngleMoy.getAvg());

} // Fin Setup


//
// Bloc d'instruction principal
//

void loop() {
  //Serial.println(F("Dans le loop."));

  //Lire les données du HC06 (Bluetooth)
  // Le programme fonctionne même s'il n'y a pas de circuit bluetooth cablé
  while (hc06.available() > 0) {
    cmdBT += (char)hc06.read();
    Serial.print("cmdBT : ");
    Serial.println(cmdBT);
  }

  //
  // Déclenchement de : stop manuel
  //
  // Si appui sur le bouton "stop"
  if (((digitalRead(BoutonStop) == LOW) && (digitalRead(BoutonEnregPos) == HIGH)) || (cmdBT == "STO")) { // Oblige de mettre la condition sur BoutonEnregPos, pour éviter de confondre avec l'option "Horizontalisation"
    Serial.println(F("Bouton Stop enfonce."));

    StopperVerins ();
    HorizontalisationenCours = 0;

    // Boucle  pour remplir le tableau  (et mettre à jour la hauteur du plateau)
    for ( byte i = 1; i <= 16; i++) {
      long DistMoy = FaireMesureUltraSon ();
      delay(20);
    }

    // Boucle  pour remplir le tableau  (et mettre à jour l'angle du plateau)
    for ( byte i = 1; i <= 16; i++) {
      LectureInclinometre ();
      delay(20);
    }

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Déclenchement de : horizontalisation automatique (A l'aide du capteur d'inclinaison)
  //
  // Si appui sur les boutons "Enregistrement Position et Stop"
  if (((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonStop) == LOW))  || (cmdBT == "HOA")) {
    Serial.println(F("Bouton enregistrement position et STOP enfonce."));

    HorizontalisationenCours = 1; // Activer la variable transitoire pour enclencher la mise à l'horizontal du plateau

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Déclenchement de : Horizontalisation automatique en cours
  //
  // Si la mise à l'horizontale du plateau a été sélectionnée
  if (HorizontalisationenCours == 1) {
    Serial.println(F("Mise à l'horizontal en cours."));

    Horizontalisation ();
  }

  //
  // Déclenchement de : Manuel Haut
  //
  // Si appui sur le bouton "Manuel Haut" (sans appui simultané sur le bouton enregistrement position)
  if (((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManHaut) == LOW))  || (cmdBT == "MAH")) {
    Serial.println(F("Bouton vers le Haut enfonce."));

    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse,vitesse);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Déclenchement de : Manuel Bas
  //
  // Si appui sur le bouton "Manuel Bas" (sans appui simultané sur le bouton enregistrement position)
  if (((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManBas) == LOW))  || (cmdBT == "MAB")) {
    Serial.println(F("Bouton vers le Bas enfonce."));

    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse,vitesse);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Déclenchement de : vers la position haute pré-réglée
  //
  // Si appui sur le bouton "Vers Position Haute" (sans appui simultané sur le bouton enregistrement position)
  if (((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonVersPosHaut) == LOW))  || (cmdBT == "AUH")) {
    Serial.println(F("Bouton vers position haute enfonce."));

    // Variable transtoire plateau en mouvement vers position pré-réglée
    Var_VersPosH_enCours = 1;

    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse,vitesse);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Vérification de la hauteur du plateau, arret lorsqu'on atteint la position haute mémorisée
  if (Var_VersPosH_enCours == 1 ) {
    Serial.println(F("En mouvement vers position Haute."));

    long DistMoy = FaireMesureUltraSon ();
    MoyA = LectureInclinometre ();

    Serial.print(F(" Différence hauteur plateau vers Haut:"));
    Serial.println(NVRAMReadlong(AddReglageHaut)  - DistMoy);

    int DiffH = NVRAMReadlong(AddReglageHaut) - DistMoy; // Variable provisoire indiquant la distance encore à parcourir

    if (DiffH > 20) {
      // Ajout d'une fonction pour modifier les vitesses des vérins en fonction de l'inclinaison du plateau
      if ( MoyA > 5 ) {
        vitesseG = 255;
        vitesseD = 195;
        ActiverVerins (vitesseG, vitesseD);
      } else if ( MoyA < 5 ) {
        vitesseG = 195;
        vitesseD = 255;
        ActiverVerins (vitesseG, vitesseD);
      } else {
        vitesse = 255;
        ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse normale
      }
      if (( DiffH > 0 ) && (DiffH < 20 )) {
        Serial.println(F(" On se rapproche de la position enregistrée. Ralentir."));
       // vitesse = 190;  // Vitesse faible : 190 en montant, 100 en descendant
       // Ajout d'une fonction pour modifier les vitesses des vérins en fonction de l'inclinaison du plateau
      if ( MoyA > 5 ) {
        vitesseG = 190;
        vitesseD = 170;
        ActiverVerins (vitesseG, vitesseD);
      } else if ( MoyA < 5 ) {
        vitesseG = 170;
        vitesseD = 190;
        ActiverVerins (vitesseG, vitesseD);
      } else {
        vitesse = 190;
        ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse normale
      }       
      //ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse faible en approchant de la hauteur
      }
    }
    if (DiffH < 0) {
      Serial.println(F("Position Haute mémorisée atteinte."));

      StopperVerins ();

      HorizontalisationenCours = 1; // Appel de la fonction horizontalisation avant arret définitif des vérins
    }
  }

  //
  // Déclenchement de : vers la position basse pré-réglée
  //
  // Si appui sur le bouton "Vers Position Basse" (sans appui simultané sur le bouton enregistrement position)
  if (((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonVersPosBas) == LOW))  || (cmdBT == "HAB")) {
    Serial.println(F("Bouton vers position Basse enfonce."));

    // Variable transtoire plateau en mouvement vers position pré-réglée
    Var_VersPosB_enCours = 1;

    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse,vitesse);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Vérification de la hauteur du plateau, arret lorsqu'on atteint la position basse mémorisée
  if (Var_VersPosB_enCours == 1 ) {
    Serial.println(F("En mouvement vers position Basse."));

    long DistMoy = FaireMesureUltraSon ();
    MoyA = LectureInclinometre ();

    Serial.print(F(" Différence hauteur plateau vers Bas:"));
    Serial.println(DistMoy - NVRAMReadlong(AddReglageBas));

    int DiffB = DistMoy - NVRAMReadlong(AddReglageBas); // Variable provisoire indiquant la distance encore à parcourir

    if (DiffB > 20) {
       // Ajout d'une fonction pour modifier les vitesses en fonction de l'inclinaison du plateau
       if ( MoyA > 5 ) {
        vitesseG = 170;
        vitesseD = 255;
        ActiverVerins (vitesseG, vitesseD);
      } else if ( MoyA < 5 ) {
        vitesseG = 255;
        vitesseD = 170;
        ActiverVerins (vitesseG, vitesseD);
      } else {
        vitesse = 255;
        ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse normale
      }
    }
    if (( DiffB > 0 ) && (DiffB < 20 )) {
      Serial.println(F(" On se rapproche de la position enregistrée. Ralentir."));
      //vitesse = 100; // Vitesse faible : 190 en montant, 100 en descendant
       // Ajout d'une fonction pour modifier les vitesses en fonction de l'inclinaison du plateau
       if ( MoyA > 5 ) {
        vitesseG = 80;
        vitesseD = 100;
        ActiverVerins (vitesseG, vitesseD);
      } else if ( MoyA < 5 ) {
        vitesseG = 100;
        vitesseD = 80;
        ActiverVerins (vitesseG, vitesseD);
      } else {
        vitesse = 100;
        ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse normale
      }
      //ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse faible en approchant de la hauteur
    }

    if (DiffB < 0) {
      Serial.println(F("Position Basse mémorisée atteinte."));

      StopperVerins ();

      HorizontalisationenCours = 1;// Appel de la fonction horizontalisation avant arret définitif des vérins
    }
  }

  //
  // Déclenchement de : 1 seul vérin en mouvement (vérin vers bas)
  //
  // Si les boutons "enregister position" et "position basse manuelle" sont enfoncés en même temps,
  if (((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManBas) == LOW))  || (cmdBT == "UVB")) {
    Serial.println(F("Bouton Enregistrement position+Position BASSE manuelle enfonce."));
    // mettre en mouvement un seul vérin (car les vérins ne vont pas à la même vitesse et le bureau devient penché
    //Mettre le vérin num 1 dans le sens descente
    digitalWrite(BorcheSensV1a, LOW);
    digitalWrite(BorcheSensV1b, HIGH);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse,vitesse);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Déclenchement de : 1 seul vérin en mouvement (vérin vers haut)
  //
  // Si les boutons "enregister position" et "position haute manuelle" sont enfoncés en même temps,
  if (((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManHaut) == LOW))  || (cmdBT == "UVH")) {
    Serial.println(F("Bouton Enregistrement position+Position HAUTE manuelle enfonce."));
    // mettre en mouvement un seul vérin (car les vérins ne vont pas à la même vitesse et le bureau devient penché
    //Mettre le vérin num 1 dans le sens montée
    digitalWrite(BorcheSensV1a, HIGH);
    digitalWrite(BorcheSensV1b, LOW);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse,vitesse);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Déclenchement de : enregistrement position haute
  //
  // Si les boutons "enregister position" et "vers position haute" sont enfoncés en même temps,
  // enregistrer la position actuelle dans la mémoire du DS1307
  if (((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosHaut) == LOW))  || (cmdBT == "EPH")) {
    Serial.println(F("Bouton Enregistrement position+vers Position Haute enfonce."));

    EnregistrerHauteur(AddReglageHaut);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }

  //
  // Déclenchement de : enregistrement position basse
  //
  // Si les boutons "enregister position" et "vers position basse" sont enfoncés en même temps,
  // enregistrer la position actuelle dans la mémoire du DS1307
  if (((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosBas) == LOW))  || (cmdBT == "EPB")) {
    Serial.println(F("Bouton Enregistrement position+vers Position BASSE enfonce."));

    EnregistrerHauteur(AddReglageBas);

    cmdBT = ""; // Réinitialisation du buffer provenant du bluetooth
  }


  delay (100);
}
// Fin du Loop
