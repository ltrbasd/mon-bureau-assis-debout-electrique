//
// Bureau assis debout individuel électrique à vérins
//
// Scanner I2C (vérification de la disponibilité du circuit RTC pour stockage des hauteurs mémorisées)
//
// Adapted to be as simple as possible by Arduino.cc user Krodal
// June 2012
//
// Traduction Juillet 2021.

#include <Wire.h>


void setup()
{
  Wire.begin();

  Serial.begin(9600);
  Serial.println("Scanner I2C");
  Serial.println("");

  byte error, address;
  int nDevices;

  Serial.println("Scan...");

  nDevices = 0;
  for (address = 0; address <= 127; address++ )
  {
    //  Le scanner I2C utilise la valeur retournée
    //  par la fin du composant Write.endTransmission
    // pour vérifier si un composant retrourne une réponse valable.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("Composant I2C trouvé à l'adresse 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println(" !");

      nDevices++;
    }
    else if (error == 4)
    {
      Serial.print("Erreur inconnue à l'adresse 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (nDevices == 0)
    Serial.println("Pas de composants I2C trouvé\n");
  else
    Serial.println("fini\n");
}


void loop()
{



}
