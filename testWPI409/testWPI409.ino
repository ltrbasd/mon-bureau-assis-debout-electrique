//
// Bureau assis debout individuel électrique à vérins
//
// Reprise du programme de test officiel du shield WPI409 (avec debug surle port série)
//
//


int in1 = 6;
int in2 = 7;
int in3 = 8;
int in4 = 9;

int SPA = 10;
int SPB = 11;

void setup() {
  Serial.begin(9600);


  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  Serial.println("Debut");

  digitalWrite(in1, HIGH);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, HIGH);



}

void loop() {
  Serial.println("2 moteurs droite");
  _mRight(in1, in2);
  _mRight(in3, in4);

  int speed = 1000;
  _Setspeed(SPA, speed);
  _Setspeed(SPB, speed);
  delay(5000);

  Serial.println("2 moteurs stop");
  _mStop(in1, in2);
  _mStop(in3, in4);
  delay(2000);

  Serial.println("2 moteurs gauche");
  _mLeft(in1, in2);
  _mLeft(in3, in4);
  _Setspeed(SPA, speed);
  _Setspeed(SPB, speed);

  delay(5000);
  Serial.println("2 moteurs stop");
  _mStop(in1, in2);
  _mStop(in3, in4);
  delay(2000);

}

void _mRight (int pin1, int pin2) {
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, LOW);
}

void _mLeft (int pin1, int pin2) {
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, HIGH);
}

void _mStop (int pin1, int pin2) {
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, HIGH);
}

void _Setspeed (int Ppwm, int speedV) {
  analogWrite(Ppwm, speedV);

}
