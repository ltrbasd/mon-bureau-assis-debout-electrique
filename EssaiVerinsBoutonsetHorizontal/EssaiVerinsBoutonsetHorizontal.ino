//
// Programme de commande du bureau assis debout
//
// Test avec les vérins branchés et valeurs du capteurs inclinaison
// (sans mesures ultra son et enregistremment des positions)
//

// Bibilothèque pour avoir une moyenne glissante lors des mesures de distance
// Non indispensable (peut ne pas être utilisée [Attention aux incohérences] ou être réalisée manuellement dans la fonction
#include <movingAvg.h>                  // https://github.com/JChristensen/movingAvg


// Fichier contenant l'affectation globale des broches de la carte ARDUINO
#include "AffectationBroches.h"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le shield moteur + constantes et variables
#include "CommandeVerins.h"

// Fichier contenant les fonctions de commande des vérins
#include "Verins.hpp"

// Fichier contenant les constantes pour la gestion de l'inclinomètre (VMA208)
#include "Horizontalisation.h"

// Fichier contenant les fonctions pour la gestion de l'inclinomètre (VMA208)
#include "Horizontalisation.hpp"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le capteur ultra son + constantes et variables
#include "UltraSon.h"

// Fichier contenant les fonctions pour le capteur ultra son
#include "UltraSon.hpp"


//
//
// Bloc de définition initial
void setup() {
  /* Initialise le port série */
  Serial.begin(9600);

  Serial.println("Debut vérins et horizontal.");

  inclin.init();

  // Définition des broches de commande des vérins en Sortie
  pinMode(BorcheSensV1a, OUTPUT);
  pinMode(BorcheSensV1b, OUTPUT);
  pinMode(BorcheSensV2a, OUTPUT);
  pinMode(BorcheSensV2b, OUTPUT);

  // On met toutes les broches de commande des vérins à HIGH pour bien s'assurer de stoper les vérins
  digitalWrite(BorcheSensV1a, HIGH);
  digitalWrite(BorcheSensV1b, HIGH);
  digitalWrite(BorcheSensV2a, HIGH);
  digitalWrite(BorcheSensV2b, HIGH);

  // Définition des broches de commande des boutons de commande en Entrée avec activation de la résistance interne de tirage
  pinMode(BoutonStop, INPUT_PULLUP);
  pinMode(BoutonVersPosHaut, INPUT_PULLUP);
  pinMode(BoutonVersPosBas, INPUT_PULLUP);
  pinMode(BoutonManHaut, INPUT_PULLUP);
  pinMode(BoutonManBas, INPUT_PULLUP);
  pinMode(BoutonEnregPos, INPUT_PULLUP);


  AngleMoy.begin();  // initialize the moving average
  AngleMoy.reading(1); // Remplir le tableau de mesure avec des valeurs
  AngleMoy.reading(2);
  AngleMoy.reading(3);
  AngleMoy.reading(4);
  AngleMoy.reading(5);
  AngleMoy.reading(6);
  AngleMoy.reading(7);
  AngleMoy.reading(8);
  AngleMoy.reading(9);
  AngleMoy.reading(10);

  AngleMoyMoy.begin();  // initialize the moving average

}

//
// Bloc d'instruction principal
//

void loop() {
  //Serial.println("Dans le loop.");

  LectureInclinometre ();

  // Si appui sur les boutons "Enregistrement Position et Stop"
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonStop) == LOW)) {

    Serial.println("Bouton enregistrement position et STOP enfonce.");

    HorizontalisationenCours = 1; // Activer la variable transitoire pour enclencher la mise à l'horizontale du plateau

    // Boucle initiale pour remplir le tableau  (et connaitre l'inclinaison initiale du plateau)
    for ( int i = 1; i <= 16; i++) {
      if (inclin.available())
      {
        inclin.read(); // Lire la sortie du capteur d'inclinaison

        float Val2 = AngleMoy.reading(inclin.z);
        int Val = Val2 * 100;
        MoyA = Val / 100 + ConstCorrectionAngle;
        Serial.print("Angle moyen initial : ");
        //Serial.print(inclin.z, 3);
        //Serial.print("\t");
        Serial.println(MoyA);

        delay(50);
      }
    }
  }

  // Si la mise à l'horizontale du plateau a été sélectionnée
  if (HorizontalisationenCours == 1) {
    Serial.println("Mise à l'horizontal en cours.");

    Horizontalisation ();
  }


  // Si appui sur le bouton "stop"
  if (digitalRead(BoutonStop) == LOW) {
    Serial.println("Bouton Stop enfonce.");
    StopperVerins ();
    HorizontalisationenCours = 0;
    // Une fois les vérins arretes, faire la mesure de l'angle
  }

  //
  // Si appui sur le bouton "Manuel Haut" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManHaut) == LOW)) {
    Serial.println("Bouton vers le Haut enfonce.");
    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  // Si appui sur le bouton "Manuel Bas" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManBas) == LOW)) {
    Serial.println("Bouton vers le Bas enfonce.");
    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si appui sur le bouton "Vers Position Haute"
  if (digitalRead(BoutonVersPosHaut) == LOW) {
    Serial.println("Bouton vers position haute enfonce.");

    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);

    // Variable transtoire
    //    Var_VersPosH_enCours = 1;
  }

  //
  // Si appui sur le bouton "Vers Position Basse"
  if (digitalRead(BoutonVersPosBas) == LOW) {
    Serial.println("Bouton vers position Basse enfonce.");

    // Variable transtoire
    //    Var_VersPosB_enCours = 1;

    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si les boutons "enregister position" et "position basse manuelle" sont enfoncés en même temps,
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManBas) == LOW)) {
    Serial.println("Bouton Enregistrement position+Position BASSE manuelle enfonce.");
    // mettre en mouvement un seul vérin (car les vérins ne vont pas à la même vitesse et le bureau devient penché
    //Mettre le vérin num 1 dans le sens descente
    digitalWrite(BorcheSensV1a, LOW);
    digitalWrite(BorcheSensV1b, HIGH);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si les boutons "enregister position" et "position haute manuelle" sont enfoncés en même temps,
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManHaut) == LOW)) {
    Serial.println("Bouton Enregistrement position+Position HAUTE manuelle enfonce.");
    // mettre en mouvement un seul vérin (car les vérins ne vont pas à la même vitesse et le bureau devient penché
    //Mettre le vérin num 1 dans le sens montée
    digitalWrite(BorcheSensV1a, HIGH);
    digitalWrite(BorcheSensV1b, LOW);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  delay (100);
}
// Fin du Loop
