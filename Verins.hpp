#ifndef verins_HPP
#define verins_HPP


// Fonctions de gestion des vérins électrique

void StopperVerins () {
  digitalWrite(BorcheSensV1a, HIGH);
  digitalWrite(BorcheSensV1b, HIGH);
  digitalWrite(BorcheSensV2a, HIGH);
  digitalWrite(BorcheSensV2b, HIGH);

  // Réinitialiser les variables transitoires liées au mouvement vers les positions pré-réglées
  Var_VersPosH_enCours = 0;
  Var_VersPosB_enCours = 0;

  // Reinitialiser la vitesse
  vitesse = 255;
}

/*void ActiverVerinsMemeVitesse (int vitesse) {
  analogWrite(BorcheVitesseV1, vitesse);
  analogWrite(BorcheVitesseV2, vitesse);
}*/

void ActiverVerins (byte vitesseG, byte vitesseD) {
  analogWrite(BorcheVitesseV1, vitesseG);
  analogWrite(BorcheVitesseV2, vitesseD);
}
void VerinsSensDescendant () {
  digitalWrite(BorcheSensV1a, LOW);
  digitalWrite(BorcheSensV1b, HIGH);
  digitalWrite(BorcheSensV2a, LOW);
  digitalWrite(BorcheSensV2b, HIGH);
}

void VerinsSensMontant () {
  digitalWrite(BorcheSensV1a, HIGH);
  digitalWrite(BorcheSensV1b, LOW);
  digitalWrite(BorcheSensV2a, HIGH);
  digitalWrite(BorcheSensV2b, LOW);
}

#endif
