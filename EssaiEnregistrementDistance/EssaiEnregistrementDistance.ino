//
// Bureau assis debout individuel électrique à vérins
//
// Programme de test de l'enregistrement et de la lecture des hauteurs dans la mémoire NVRAM du DS1307 (sans mouvement des vérins)
//
// Usage :
// - Bouton Enregistrer position + automatique haut ou automatique bas : Enregistrer la hauteur actuelle du plateau
// - Manuel Haut ou Bas : Lire les valeurs enregistrées sur le DS1307
//
// Juillet-Aout-Septembre 2021
// Licence : CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//

// Bibilothèque pour avoir une moyenne glissante lors des mesures de distance
// Non indispensable (peut ne pas être utilisée [Attention aux incohérences] ou être réalisée manuellement dans la fonction)
#include <movingAvg.h>                  // https://github.com/JChristensen/movingAvg

// Bibliothèque pour la liaison avec le DS1307 (Utilisé non en module RTC mais pour l'enregistrement des positions hautes et basses)
#include <Wire.h>

// Fichier contenant les constantes pour la gestion de la mémoire NVRAM
#include "DS1307.h"

// Fichier contenant les fonctions pour la gestion de la mémoire NVRAM
#include "DS1307.hpp"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO
#include "AffectationBroches.h"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le capteur ultra son + constantes et variables
#include "UltraSon.h"

// Fichier contenant les fonctions pour le capteur ultra son
#include "UltraSon.hpp"

//
// Bloc de définition initial
void setup() {
  // Initialise le port série
  Serial.begin(9600);

  // Phrases de présentation du programme pour le debug sur le port série
  Serial.println("Bureau Assis-Debout Electrique.");
  Serial.println("Debut enregistrement bouton distance.");

  // Initialise le port I2C
  Wire.begin();

  // Définition des broches de commande des boutons de commande en Entrée avec activation de la résistance interne de tirage
  pinMode(BoutonStop, INPUT_PULLUP);
  pinMode(BoutonVersPosHaut, INPUT_PULLUP);
  pinMode(BoutonVersPosBas, INPUT_PULLUP);
  pinMode(BoutonManHaut, INPUT_PULLUP);
  pinMode(BoutonManBas, INPUT_PULLUP);
  pinMode(BoutonEnregPos, INPUT_PULLUP);

  //
  // Partie capteur ultra son
  // initialise l'objet pour la mesure de distance moyenne
  DistM.begin();

  //  Initialise les broches pour le capteur ultrason
  pinMode(BROCHE_TRIGGER, OUTPUT);
  digitalWrite(BROCHE_TRIGGER, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(BROCHE_ECHO, INPUT);

  // Pré-Remplir le tableau de mesures par ultra son pour avoir immédiatement la moyenne des distances
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();

  // Lecture sur la NVRAM des valeurs de réglage du plateau enregistrées
  // Prévoir quelque chose pour vérifier que la variable a bien été intialisée ? (Ne semble pas utile, les fins de courses des vérins devraient suffir))
  long ValEnrPosBasse = NVRAMReadlong(AddReglageBas);
  long ValEnrPosHaute = NVRAMReadlong(AddReglageHaut);

  Serial.println(ValEnrPosHaute);
  Serial.println(ValEnrPosBasse);
}

//
// Bloc d'instruction principal
//

void loop() {
  //Serial.println("Dans le loop.");

  //
  // Si appui sur le bouton "Manuel Haut"
  if (digitalRead(BoutonManHaut) == LOW) {
    Serial.println("Bouton vers le Haut enfonce.");
    long ValEnrPosHaute = NVRAMReadlong (AddReglageHaut);
    Serial.println(ValEnrPosHaute);
  }

  // Si appui sur le bouton "Manuel Bas"
  if (digitalRead(BoutonManBas) == LOW) {
    Serial.println("Bouton vers le Bas enfonce.");
    long ValEnrPosBasse = NVRAMReadlong (AddReglageBas);
    Serial.println(ValEnrPosBasse);
  }

  //
  // Si les boutons "enregister position" et "vers position haute" sont enfoncés en même temps,
  // enregistrer la position actuelle dans la mémoire du DS1307
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosHaut) == LOW)) {
    Serial.println("Bouton Enregistrement position+vers Position Haute enfonce.");

    EnregistrerHauteur(AddReglageHaut);
  }

  // Si les boutons "enregister position" et "vers position basse" sont enfoncés en même temps,
  // enregistrer la position actuelle dans la mémoire du DS1307
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosBas) == LOW)) {
    Serial.println("Bouton Enregistrement position+vers Position BASSE enfonce.");

    EnregistrerHauteur(AddReglageBas);
  }

  delay (100);
}
// Fin du Loop
