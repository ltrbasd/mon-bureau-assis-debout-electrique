#ifndef UltraSon_HPP
#define UltraSon_HPP


long Mesure_ultra_Son_instantanee () {
  const byte BROCHE_TRIGGER = 4;
  const byte BROCHE_ECHO = 5;

    /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
  digitalWrite(BROCHE_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(BROCHE_TRIGGER, LOW);

  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure = pulseIn(BROCHE_ECHO, HIGH, TIMEOUT);

  // 3. Calcul la distance à partir du temps mesuré
  long distance_mm = measure / 2.0 * VITESSE_SON;
  //Serial.print(F("Distance instantanée NON corrigée: "));
  //Serial.println(distance_mm);

  return (distance_mm);
}


// Fonction de gestion de la partie mesure de la distance au sol via le capteur ultra-son
//
long FaireMesureUltraSon () {
  
long DistMoy;
//long distance_mm;
long ecart_type = 0;

  //const byte BROCHE_TRIGGER = 4;
  //const byte BROCHE_ECHO = 5;

  // 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER
  digitalWrite(BROCHE_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(BROCHE_TRIGGER, LOW);

  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure = pulseIn(BROCHE_ECHO, HIGH, TIMEOUT);

  // 3. Calcul la distance à partir du temps mesuré
  long distance_mm = measure / 2.0 * VITESSE_SON;

//  ecart_type = sqrt((distance_mm - DistMoy) * (distance_mm - DistMoy) / 7 );

//
// Fonction d'élimination des valleurs aberrantes

// Seuil de déclenchement des valeurs aberrantes à régler

  if (ecart_type > 50) {
// Permet d'ignorer les valeurs très aberrantes (inscrit la moyenne actuelle au lieu de la valeur aberrante)    
    DistMoy = DistM.reading(DistM.getAvg());
    //Serial.print(F("Ecart type > 100:"));
    // Serial.println(avg);

// Action a réaliser s'il y a trop de valeurs aberrantes (Sécurité ?)
    i++;
    if (i > 10) { // S'il y a plus de 10 valeurs aberrantes
      Serial.println("STOP");
      i = 0;
    }
    /*
      } else if (ecart_type > 10) {
// Permet de d'atténuer les valeurs pas trop aberrantes (Enlève la moitié de l'écart à la moyenne de la dernière mesure instantané avant moyennage)
        float distance;
        if (distance_mm > avg) {
          //float ecart_moyenne1 = (distance_mm - avg) / 2;
          distance = distance_mm - ((distance_mm - avg) / 2) ;
          //distance = distance_mm - ecart_moyenne1 ;
          //Serial.print(distance_mm);
          //Serial.print(F(",Correction distance >: "));
          //Serial.println(ecart_moyenne1);
        } else {
          // if (distance_mm < avg) {
          //float ecart_moyenne2 = (avg - distance_mm) / 2;
          //distance = distance_mm + ecart_moyenne2;
          distance = distance_mm + ((avg - distance_mm) / 2);
          //Serial.print(distance);
          //Serial.print(F(",Correction distance <:"));
          //Serial.println(ecart_moyenne2);
        }
        //Serial.print(F("Distance instantanée corrigée: "));
        //Serial.println(distance_mm);
        avg = avgTemp.reading(distance);             // Lit une nouvelle valeur de distance et renvoi la moyenne

    */
  } else {
    DistMoy = DistM.reading(distance_mm);             // Lit une nouvelle valeur de distance et renvoi la moyenne
    i = 0; // Réinitialisation de la variable si les mesures ne sont plus aberrantes (implique X mesures aberrantes d'afillées)
  }





  // 4. Rempli le tableau moyenne avec  une nouvelle valeur de distance et renvoi la moyenne calculée
 // DistMoy = DistM.reading(distance_mm);

  Serial.print(F("Distance instantanée: "));
  Serial.print(distance_mm);

/*
  // La bibliothèque de moyennage ne retournant que des zéros après la virgule
  // Le résultat est multiplié par 100
  long Val = DistMoy * 100;
  // puis redivisé par 100
  long Val2 = Val / 100;
  // pour faire disparaitre les 2 chiffres après la virgule
  // Ce qui évite les problèmes de compatibilité entre type float (AVEC des chiffres après la virgule) et type long (SANS chiffres après la virgule)
*/
  Serial.print(F(", Distance Moyenne 8: "));
  Serial.print(DistMoy);
 Serial.print(F(", Ecartype : "));
  Serial.println(ecart_type);

  return (DistMoy);
}

//
// Fonction de mise en forme  et enregistrement de la hauteur de réglage dans la NVRAM du DS1307
//

void EnregistrerHauteur (char AddReglage) {

  // Faire 3 mesures de distance pour mise à jour avant enregistrement
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  long Val = FaireMesureUltraSon ();

  // Ecritue surla NVRAM
  byte Res = NVRAMWritelong(AddReglage, Val);
  delay (50);
}

#endif
