#ifndef AffectationBroches_H
#define AffectationBroches_H

// Constantes pour les broches des boutons de commande
const char BoutonStop = A0;
const char BoutonVersPosHaut = A1;
const char BoutonVersPosBas = A2;
// Je passe les 2 boutons manuels sur des ports numériques car les broches analogique A4 et A5 sur ma carte sont HS.
const char BoutonManHaut = 2; 
const char BoutonManBas = 3;
const char BoutonEnregPos = A3;

#endif 
