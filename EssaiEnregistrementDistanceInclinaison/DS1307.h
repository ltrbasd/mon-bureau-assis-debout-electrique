#ifndef DS1307_H
#define DS1307_H

// Bibliothèque pour la liaison avec le DS1307 (Utilisé non en module RTC mais pour l'enregistrement des positions hautes et basses)
#include <Wire.h>

/** Adresse I2C du module RTC DS1307 */
const uint8_t DS1307_ADDRESS = 0x68;

/** Adresse du registre de contrôle du module RTC DS1307 */
const uint8_t DS1307_CTRL_REGISTER = 0x07;

/** Adresse et taille de la NVRAM du module RTC DS1307 */
const uint8_t DS1307_NVRAM_BASE = 0x08;
const uint8_t DS1307_NVRAM_SIZE = 56;

#endif /* DS1307_H */
