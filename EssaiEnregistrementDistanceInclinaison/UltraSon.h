#ifndef UltraSon_H
#define UltraSon_H

// Constantes pour le capteur ultra son
const byte BROCHE_TRIGGER = 4; // Broche TRIGGER
const byte BROCHE_ECHO = 5;    // Broche ECHO

// Constante pour le timeout 
const unsigned long TIMEOUT = 15000UL; // 25ms = ~8m à 340m/s

// Vitesse du son dans l'air en mm/us 
const float VITESSE_SON = 344.0 / 1000;

// Constantes des @ de stockage des réglages de hauteur mémorisés
const char AddReglageHaut = 0;
const char AddReglageBas = 20;

// Définition de l'objet Mesure_Moyenne+nombre de mesures
movingAvg DistM(8);

char i = 0;

#endif 
