//
// Bureau Assis-Debout Electrique
//
// Programme de test pour actionner manuellement les vérins avec debug sur le port série
//
// Usage :
// - Bouton stop : Arret du/des vérins 
// - Manuel haut ou bas et automatique haut ou bas : Mise en marche des 2 vérins dans le sens demandé jusqu'à arret manuel
// - Enregistrement position + manuel haut ou bas : Mise en marche du seul vérin gauche pour horizontalisation manuelle du plateau
//
// Juillet-Aout-Septembre 2021
// Licence : CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//

// Fichier contenant l'affectation globale des broches de la carte ARDUINO
#include "AffectationBroches.h"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le shield moteur + constantes et variables
#include "CommandeVerins.h"

// Fichier contenant les fonctions de commande des vérins
#include "Verins.hpp"

//
// Bloc de définition initial
void setup() {
  // Initialise le port série
  Serial.begin(9600);

  // Phrases de présentation du programme pour le debug sur le port série
  Serial.println("Bureau Assis-Debout Electrique.");
  Serial.println("Début Essai vérins et boutons.");

  // Définition des broches de commande des vérins en Sortie
  pinMode(BorcheSensV1a, OUTPUT);
  pinMode(BorcheSensV1b, OUTPUT);
  pinMode(BorcheSensV2a, OUTPUT);
  pinMode(BorcheSensV2b, OUTPUT);

  // On met toutes les broches de commande des vérins à HIGH pour bien s'assurer de stoper les vérins
  digitalWrite(BorcheSensV1a, HIGH);
  digitalWrite(BorcheSensV1b, HIGH);
  digitalWrite(BorcheSensV2a, HIGH);
  digitalWrite(BorcheSensV2b, HIGH);

  // Définition des broches de commande des boutons de commande en Entrée avec activation de la résistance interne de tirage
  pinMode(BoutonStop, INPUT_PULLUP);
  pinMode(BoutonVersPosHaut, INPUT_PULLUP);
  pinMode(BoutonVersPosBas, INPUT_PULLUP);
  pinMode(BoutonManHaut, INPUT_PULLUP);
  pinMode(BoutonManBas, INPUT_PULLUP);
  pinMode(BoutonEnregPos, INPUT_PULLUP);
}

//
// Bloc d'instruction principal
//

void loop() {
  //Serial.println("Dans le loop.");

  // Si appui sur le bouton "stop"
  if (digitalRead(BoutonStop) == LOW) {
    Serial.println("Bouton Stop enfonce.");
    StopperVerins ();
  }

  //
  // Si appui sur le bouton "Manuel Haut" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManHaut) == LOW)) {
    Serial.println("Bouton vers le Haut enfonce.");
    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si appui sur le bouton "Manuel Bas" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManBas) == LOW)) {
    Serial.println("Bouton vers le Bas enfonce.");
    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si appui sur le bouton "Vers Position Haute"
  if (digitalRead(BoutonVersPosHaut) == LOW) {
    Serial.println("Bouton vers position haute enfonce.");

    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);

    // Variable transtoire
    Var_VersPosH_enCours = 1;
  }

  //
  // Si appui sur le bouton "Vers Position Basse"
  if (digitalRead(BoutonVersPosBas) == LOW) {
    Serial.println("Bouton vers position Basse enfonce.");

    // Variable transtoire
    Var_VersPosB_enCours = 1;

    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Fonction ajoutée car les 2 vérins ne vont pas à la même vitesse et le bureau penche. Permet de ne mettre en mouvement qu'un seul vérin
  //
  // Si les boutons "enregister position" et "position basse manuelle" sont enfoncés en même temps,
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManBas) == LOW)) {
    Serial.println("Bouton Enregistrement position+Position BASSE manuelle enfonce.");
    //Mettre le vérin num 1 dans le sens descente
    digitalWrite(BorcheSensV1a, LOW);
    digitalWrite(BorcheSensV1b, HIGH);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si les boutons "enregister position" et "position haute manuelle" sont enfoncés en même temps,
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManHaut) == LOW)) {
    Serial.println("Bouton Enregistrement position+Position HAUTE manuelle enfonce.");
    //Mettre le vérin num 1 dans le sens montée
    digitalWrite(BorcheSensV1a, HIGH);
    digitalWrite(BorcheSensV1b, LOW);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  delay (100);
}
// Fin du Loop
