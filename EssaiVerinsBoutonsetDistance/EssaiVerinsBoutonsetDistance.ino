//
// Bureau Assis-Debout Electrique
//
// Programme avec les vérins actifs et la mesure + sauvegarde des distances (aller vers une position pré-réglée)
//
// Usage :
// - Bouton stop : Arret du/des vérins + boucle pour mise à jour de la hauteur du plateau (ultra son)
// - Manuel haut ou bas et automatique haut ou bas : Mise en marche des 2 vérins dans le sens demandé jusqu'à arret manuel
// - Bouton Enregistrer position + automatique haut ou automatique bas : Enregistrer la hauteur actuelle du plateau
// - Automatique haut ou bas : Mise en marche des 2 vérins dans le sens demandé jusqu'à la hauteur enregistrée dans la mémoire du DS 1307
// - Enregistrement position + manuel haut ou bas : Mise en marche du seul vérin gauche pour horizontalisation manuelle du plateau
//
// Juillet-Aout-Septembre 2021
// Licence : CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//

// Bibilothèque pour avoir une moyenne glissante lors des mesures de distance
// Non indispensable (peut ne pas être utilisée [Attention aux incohérences] ou être réalisée manuellement dans la fonction
#include <movingAvg.h>                  // https://github.com/JChristensen/movingAvg

// Bibliothèque pour la liaison avec le DS1307 (Utilisé non en module RTC mais pour l'enregistrement des positions hautes et basses)
#include <Wire.h>

// Fichier contenant les constantes pour la gestion de la mémoire NVRAM
#include "DS1307.h"

// Fichier contenant les constantes pour la gestion de la mémoire NVRAM
#include "DS1307.hpp"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO
#include "AffectationBroches.h"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le shield moteur + constantes et variables
#include "CommandeVerins.h"

// Fichier contenant les fonctions de commande des vérins
#include "Verins.hpp"

// Fichier contenant l'affectation globale des broches de la carte ARDUINO pour le capteur ultra son + constantes et variables
#include "UltraSon.h"

// Fichier contenant les fonctions pour le capteur ultra son
#include "UltraSon.hpp"


//
//
// Bloc de définition initial
void setup() {
  // Initialise le port série
  Serial.begin(9600);

  // Phrases de présentation du programme pour le debug sur le port série
  Serial.println("Bureau Assis-Debout Electrique.");
  Serial.println("Début programme de test vérins et enregistrement distance.");

  // Initialise le port I2C
  Wire.begin();

  // Définition des broches de commande des vérins en Sortie
  pinMode(BorcheSensV1a, OUTPUT);
  pinMode(BorcheSensV1b, OUTPUT);
  pinMode(BorcheSensV2a, OUTPUT);
  pinMode(BorcheSensV2b, OUTPUT);

  // On met toutes les broches de commande des vérins à HIGH pour bien s'assurer de stoper les vérins
  digitalWrite(BorcheSensV1a, HIGH);
  digitalWrite(BorcheSensV1b, HIGH);
  digitalWrite(BorcheSensV2a, HIGH);
  digitalWrite(BorcheSensV2b, HIGH);

  // Définition des broches de commande des boutons de commande en Entrée avec activation de la résistance interne de tirage
  pinMode(BoutonStop, INPUT_PULLUP);
  pinMode(BoutonVersPosHaut, INPUT_PULLUP);
  pinMode(BoutonVersPosBas, INPUT_PULLUP);
  pinMode(BoutonManHaut, INPUT_PULLUP);
  pinMode(BoutonManBas, INPUT_PULLUP);
  pinMode(BoutonEnregPos, INPUT_PULLUP);

  //
  // Partie capteur ultra son
  // initialise l'objet pour la mesure de distance moyenne
  DistM.begin();

  //  Initialise les broches pour le capteur ultrason
  pinMode(BROCHE_TRIGGER, OUTPUT);
  digitalWrite(BROCHE_TRIGGER, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(BROCHE_ECHO, INPUT);

  // Pré-Remplir le tableau de mesures par ultra son pour avoir immédiatement la moyenne des distances
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();

  // Lire les valeurs enregistrées dans la NVRAM pour le debug
  long ValEnrPosBasse = NVRAMReadlong(AddReglageBas);
  long ValEnrPosHaute = NVRAMReadlong(AddReglageHaut);
  Serial.println(ValEnrPosBasse);
  Serial.println(ValEnrPosHaute);

}

//
// Bloc d'instruction principal
//

void loop() {
  //Serial.println("Dans le loop.");

  //float DistMoy = FaireMesureUltraSon ();

  // Si appui sur le bouton "stop"
  if (digitalRead(BoutonStop) == LOW) {
    Serial.println("Bouton Stop enfonce.");
    StopperVerins ();

    // Boucle pour remplir le tableau  (et connaitre la hauteur du plateau)
    for ( int i = 1; i <= 8; i++) {
      long DistMoy = FaireMesureUltraSon ();

      delay(30);
    }
  }



  //
  // Si appui sur le bouton "Manuel Haut" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManHaut) == LOW)) {
    Serial.println("Bouton vers le Haut enfonce.");
    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  // Si appui sur le bouton "Manuel Bas" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManBas) == LOW)) {
    Serial.println("Bouton vers le Bas enfonce.");
    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si les boutons "enregister position" et "vers position haute" sont enfoncés en même temps,
  // enregistrer la position actuelle dans la mémoire du DS1307
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosHaut) == LOW)) {
    Serial.println("Bouton Enregistrement position+vers Position Haute enfonce.");

    long DistMoy = FaireMesureUltraSon ();
    //Serial.print(F("Nouvelle valeur Haute : "));
    //Serial.println(DistMoy);
    long Val = DistMoy * 100;
    //Serial.print(F("Nouvelle valeur Basse VAL: "));
    //Serial.println(Val);
    long Val2 = Val / 100;
    Serial.print(F("Nouvelle valeur Basse VAL2: "));
    Serial.println(Val2);

    byte Res = NVRAMWritelong(AddReglageHaut, Val2);
    delay (50);
  }

  // Si les boutons "enregister position" et "vers position basse" sont enfoncés en même temps,
  // enregistrer la position actuelle dans la mémoire du DS1307
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosBas) == LOW)) {
    Serial.println("Bouton Enregistrement position+vers Position BASSE enfonce.");
    long DistMoy = FaireMesureUltraSon ();
    //Serial.print(F("Nouvelle valeur Basse : "));
    //Serial.println(DistMoy);
    long Val = DistMoy * 100;
    //Serial.print(F("Nouvelle valeur Basse VAL: "));
    //Serial.println(Val);
    long Val2 = Val / 100;
    Serial.print(F("Nouvelle valeur Basse VAL2: "));
    Serial.println(Val2);

    byte Res = NVRAMWritelong(AddReglageBas, Val2);
    delay (50);
  }

  //
  // Si appui sur le bouton "Vers Position Haute" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonVersPosHaut) == LOW)) {
    Serial.println("Bouton vers position haute enfonce.");

    //Mettre les 2 vérins dans le sens montée
    VerinsSensMontant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);

    // Variable transtoire
    Var_VersPosH_enCours = 1;
  }

  //
  // Vérification de la hauteur du plateau, arret lorsqu'on atteint la position haute mémorisée
  if (Var_VersPosH_enCours == 1 ) {
    Serial.println("En mouvement vers position Haute.");

    long DistMoy = FaireMesureUltraSon ();
    
    Serial.print(" Différence hauteur plateau vers Haut:");
    Serial.println(NVRAMReadlong(AddReglageHaut)  - DistMoy);

    int DiffH = NVRAMReadlong(AddReglageHaut) - DistMoy; // Variable provisoire indiquant la distance encore à parcourir

    if (DiffH > 10) {
      vitesse = 255;
      ActiverVerins (vitesse); // Relancer la commande avec la vitesse normale
    }
    if (( DiffH > 0 ) && (DiffH < 10 )) {
      Serial.println(" On se rapproche de la position enregistrée. Ralentir.");
      vitesse = 160;
      ActiverVerins (vitesse); // Relancer la commande avec la vitesse faible en approchant de la hauteur
    }

    if (DiffH < 0) {
      Serial.println("Position Haute mémorisée atteinte.");
      StopperVerins ();
    }
  }

  //
  // Si appui sur le bouton "Vers Position Basse"  (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonVersPosBas) == LOW)) {
    Serial.println("Bouton vers position Basse enfonce.");

    // Variable transtoire
    Var_VersPosB_enCours = 1;

    //Mettre les 2 vérins dans le sens descente
    VerinsSensDescendant ();

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Vérification de la hauteur du plateau, arret lorsqu'on atteint la position basse mémorisée
  if (Var_VersPosB_enCours == 1 ) {
    Serial.println("En mouvement vers position Basse.");

    long DistMoy = FaireMesureUltraSon ();
    
    Serial.print(" Différence hauteur plateau vers Bas:");
    Serial.println(DistMoy - NVRAMReadlong(AddReglageBas));

    int DiffH = DistMoy - NVRAMReadlong(AddReglageBas); // Variable provisoire indiquant la distance encore à parcourir

    if (DiffH > 10) {
      vitesse = 255;
      ActiverVerins (vitesse); // Relancer la commande avec la vitesse normale
    }
    if (( DiffH > 0 ) && (DiffH < 10 )) {
      Serial.println(" On se rapproche de la position enregistrée. Ralentir.");
      vitesse = 100;
      ActiverVerins (vitesse); // Relancer la commande avec la vitesse faible en approchant de la hauteur
    }

    if (DiffH < 0) {
      Serial.println("Position Basse mémorisée atteinte.");
      StopperVerins ();
    }
  }

  //
  // Si les boutons "enregister position" et "position basse manuelle" sont enfoncés en même temps,
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManBas) == LOW)) {
    Serial.println("Bouton Enregistrement position+Position BASSE manuelle enfonce.");
    // mettre en mouvement un seul vérin (car les vérins ne vont pas à la même vitesse et le bureau devient penché
    //Mettre le vérin num 1 dans le sens descente
    digitalWrite(BorcheSensV1a, LOW);
    digitalWrite(BorcheSensV1b, HIGH);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }

  //
  // Si les boutons "enregister position" et "position haute manuelle" sont enfoncés en même temps,
  if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManHaut) == LOW)) {
    Serial.println("Bouton Enregistrement position+Position HAUTE manuelle enfonce.");
    // mettre en mouvement un seul vérin (car les vérins ne vont pas à la même vitesse et le bureau devient penché
    //Mettre le vérin num 1 dans le sens montée
    digitalWrite(BorcheSensV1a, HIGH);
    digitalWrite(BorcheSensV1b, LOW);

    //Activer les moteurs des vérins
    ActiverVerins (vitesse);
  }


  delay (200);
}
// Fin du Loop
