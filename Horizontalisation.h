#ifndef Horizontalisation_H
#define Horizontalisation_H

// Constantes pour la mise à l'horizontal du plateau
#include <SFE_MMA8452Q.h>// Librairie de l'inclinomètre (VMA208)

MMA8452Q inclin; // initialisation d'un objet inclinomètre

// Définition de l'objet "Mesure angle du plateau Moyen"+nombre de mesures
movingAvg AngleMoy(8);

// Ne pas asser en type byte/char. Paramètres > ou < a 255.
//
int MoyA;

// Variable transitoire indiquant que la mise à l'hoizontal du plateau du bureau est en cours
boolean HorizontalisationenCours = 0;

// Constante pour la correction initiale de l'angle du plateau (lié au positionnement de l'inclinomètre sur son support (colle+soudure))
const byte ConstCorrectionAngle = 95;

#endif 
