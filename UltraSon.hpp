#ifndef UltraSon_HPP
#define UltraSon_HPP


// Fonction de gestion de la partie mesure de la distance au sol via le capteur ultra-son
// mesure brute, sans correction
//
long Mesure_ultra_Son_instantanee () {

  /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
  digitalWrite(BROCHE_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(BROCHE_TRIGGER, LOW);

  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure = pulseIn(BROCHE_ECHO, HIGH, TIMEOUT);

  // 3. Calcul la distance à partir du temps mesuré
  long distance_mm = measure / 2.0 * VITESSE_SON;
  //Serial.print(F("Distance instantanée NON corrigée: "));
  //Serial.println(distance_mm);

  return (distance_mm);
}


// Fonction de gestion de la partie mesure de la distance au sol via le capteur ultra-son
// avec moyennage de la mesure
//
long FaireMesureUltraSon () {

  // 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER
  digitalWrite(BROCHE_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(BROCHE_TRIGGER, LOW);

  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure = pulseIn(BROCHE_ECHO, HIGH, TIMEOUT);

  // 3. Calcul la distance à partir du temps mesuré
  long distance_mm = measure / 2.0 * VITESSE_SON;

  // 4. Rempli le tableau moyenne avec  une nouvelle valeur de distance et renvoi la moyenne calculée
  long DistMoy = DistM.reading(distance_mm);             // Lit une nouvelle valeur de distance et renvoi la moyenne

  Serial.print(F("Distance instantanée: "));
  Serial.print(distance_mm);

  Serial.print(F(", Distance Moyenne 8: "));
  Serial.println(DistMoy);

  return (DistMoy);
}

//
// Fonction de mise en forme  et enregistrement de la hauteur de réglage dans la NVRAM du DS1307
//

void EnregistrerHauteur (char AddReglage) {

  // Faire 3 mesures de distance pour mise à jour avant enregistrement
  FaireMesureUltraSon ();
  FaireMesureUltraSon ();
  long Val = FaireMesureUltraSon ();

  // Ecriture surla NVRAM
  //byte Res = NVRAMWritelong(AddReglage, Val);
  NVRAMWritelong(AddReglage, Val);
  delay (50);
}

#endif
