//
// Bureau Assis-Debout Electrique
//
// Programme de test des boutons de commande à l'aide du debug sur le port série.
//
// Usage :
// Chaque appui sur un bouton provoque un debug sur le port série avec le nom de la fonction appelée.
// Différentes combinaisons d'appuis à vérifier :
// - Bouton stop, manuel haut, manuel bas, automatique haut, automatique bas, enregistrement position
// - Enregistrement position + automatique haut/bas
// - Enregistrement position + manuel haut/bas
// - Enregistrement position + stop
//
// Juillet-Aout-Septembre 2021
// Licence : CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//

// Fichier contenant l'affectation globale des broches de la carte ARDUINO 
#include "AffectationBroches.h"

//
// Bloc de définition initial
void setup() {
  // Initialise le port série
  Serial.begin(9600);
 
  // Phrases de présentation du programme pour le debug sur le port série
  Serial.println("Bureau Assis-Debout Electrique.");
  Serial.println("Debut Test Boutons.");

  // Définition des broches de commande des boutons de commande en Entrée avec activation de la résistance interne de tirage
  pinMode(BoutonStop, INPUT_PULLUP);
  pinMode(BoutonVersPosHaut, INPUT_PULLUP);
  pinMode(BoutonVersPosBas, INPUT_PULLUP);
  pinMode(BoutonManHaut, INPUT_PULLUP);
  pinMode(BoutonManBas, INPUT_PULLUP);
  pinMode(BoutonEnregPos, INPUT_PULLUP);

  pinMode(13, OUTPUT); //On utilise la LED de la broche 13, qu'on défini en sortie
}

//
// Bloc d'instruction principal
//

void loop() {
  //Serial.println("Dans le loop.");

  // Si appui sur le bouton "stop" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonStop) == LOW)) {
    Serial.println("Bouton Stop enfonce.");
  }

  //
  // Si appui sur le bouton "Manuel Haut" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManHaut) == LOW)) {
    Serial.println("Bouton vers le Haut enfonce.");
  }

  // Si appui sur le bouton "Manuel Bas" (sans appui simultané sur le bouton enregistrement position)
  if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonManBas) == LOW)) {
      Serial.println("Bouton vers le Bas enfonce.");
    }

    //
    // Si appui sur le bouton "Vers Position Haute" (sans appui simultané sur le bouton enregistrement position)
    if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonVersPosHaut) == LOW)) {
      Serial.println("Bouton vers position haute enfonce.");
    }

    //
    // Si appui sur le bouton "Vers Position Basse" (sans appui simultané sur le bouton enregistrement position)
    if ((digitalRead(BoutonEnregPos) == HIGH) && (digitalRead(BoutonVersPosBas) == LOW)) {
      Serial.println("Bouton vers position Basse enfonce.");
    }

    //
    // Si appui sur le bouton "EnregistrementPos"
    if (digitalRead(BoutonEnregPos) == LOW) {
      Serial.println("Bouton Enregistrement position enfonce.");
    }

    // Si les boutons "enregister position" et "vers position haute" sont enfoncés en même temps,
    if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosHaut) == LOW)) {
      Serial.println("Bouton Enregistrement position + vers Position HAUTE enfonce.");
    }

    // Si les boutons "enregister position" et "vers position basse" sont enfoncés en même temps,
    if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonVersPosBas) == LOW)) {
      Serial.println("Bouton Enregistrement position + vers Position BASSE enfonce.");
    }

    // Si les boutons "enregister position" et "manuel Haut" sont enfoncés en même temps,
    if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManHaut) == LOW)) {
      Serial.println("Bouton Enregistrement position + manuel HAUT enfonce.");
    }

    // Si les boutons "enregister position" et "manuel Bas" sont enfoncés en même temps,
    if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonManBas) == LOW)) {
      Serial.println("Bouton Enregistrement position + manuel BAS enfonce.");
    }

    if ((digitalRead(BoutonEnregPos) == LOW) && (digitalRead(BoutonStop) == LOW)) {
      // Si les boutons "enregister position" et "stop" sont enfoncés en même temps,
      Serial.println("Bouton Enregistrement position + Stop enfonce.");
    }

    delay (100);
  }
  // Fin du Loop
