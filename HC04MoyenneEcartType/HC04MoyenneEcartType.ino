// Bureau assis debout individuel électrique à vérins
//
// Programme de test de relevé moyenné avec correction en fonction de l'écart-type des retours du capteur à ultrason
//
// Adapté de Arduino Moving Average Library
// https://github.com/JChristensen/movingAvg
// Copyright (C) 2018 by Jack Christensen and licensed under
// GNU GPL v3.0, https://www.gnu.org/licenses/gpl.html
//

#include <movingAvg.h>                  // https://github.com/JChristensen/movingAvg


/* Constantes pour les broches */
const byte TRIGGER_PIN = 4; // Broche TRIGGER
const byte ECHO_PIN = 5;    // Broche ECHO


/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 15000UL; // 25ms = ~8m à 340m/s

/* Vitesse du son dans l'air en mm/us */
const float SOUND_SPEED = 344.0 / 1000;

float avg;
float distance_mm_c;

movingAvg avgTemp(8);                  // define the moving average object
char i = 0;


void setup()
{
  Serial.begin(9600);
  avgTemp.begin();                            // initialize the moving average

  /* Initialise les broches */
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(ECHO_PIN, INPUT);

avg = Mesure_ultra_Son_instantanee ();
  avgTemp.reading(avg); // Remplir le tableau de mesure avec des valeurs
  avgTemp.reading(avg);
  avgTemp.reading(avg);
  avgTemp.reading(avg);
  avgTemp.reading(avg);
  avgTemp.reading(avg);
  avgTemp.reading(avg);
  avgTemp.reading(avg);

  Serial.print("Moyenne initiale : ");
  Serial.println(avgTemp.getAvg());

}

long Mesure_ultra_Son_instantanee () {
    /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);

  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);

  // 3. Calcul la distance à partir du temps mesuré
  long distance_mm = measure / 2.0 * SOUND_SPEED;
  //Serial.print(F("Distance instantanée NON corrigée: "));
  //Serial.println(distance_mm);

  return (distance_mm);
}


void loop()
{
  //long distance_mm = 600;
/*
  // 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER 
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);

  // 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
  long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);

  // 3. Calcul la distance à partir du temps mesuré
  distance_mm = measure / 2.0 * SOUND_SPEED;
  Serial.print(F("Distance instantanée NON corrigée: "));
  Serial.println(distance_mm);
  //float distanceNC = distance_mm;
*/
 long distance_mm = Mesure_ultra_Son_instantanee ();
 
  float ecart_type = sqrt((distance_mm - avg) * (distance_mm - avg) / 7 );

  float avg_sans_correction = avgTemp.getAvg();


// Seuil de déclenchement des valeurs aberrantes à régler

  if (ecart_type > 50) {
// Permet d'ignorer les valeurs très aberrantes (inscrit la moyenne actuelle au lieu de la valeur aberrante)    
    avg = avgTemp.reading(avg_sans_correction);
    //Serial.print(F("Ecart type > 100:"));
    // Serial.println(avg);

// Action a réaliser s'il y a trop de valeurs aberrantes (Sécurité ?)
    i++;
    if (i > 10) { // S'il y a plus de 10 valeurs aberrantes
      Serial.println("STOP");
      i = 0;
    }
    /*
      } else if (ecart_type > 10) {
// Permet de d'atténuer les valeurs pas trop aberrantes (Enlève la moitié de l'écart à la moyenne de la dernière mesure instantané avant moyennage)
        float distance;
        if (distance_mm > avg) {
          //float ecart_moyenne1 = (distance_mm - avg) / 2;
          distance = distance_mm - ((distance_mm - avg) / 2) ;
          //distance = distance_mm - ecart_moyenne1 ;
          //Serial.print(distance_mm);
          //Serial.print(F(",Correction distance >: "));
          //Serial.println(ecart_moyenne1);
        } else {
          // if (distance_mm < avg) {
          //float ecart_moyenne2 = (avg - distance_mm) / 2;
          //distance = distance_mm + ecart_moyenne2;
          distance = distance_mm + ((avg - distance_mm) / 2);
          //Serial.print(distance);
          //Serial.print(F(",Correction distance <:"));
          //Serial.println(ecart_moyenne2);
        }
        //Serial.print(F("Distance instantanée corrigée: "));
        //Serial.println(distance_mm);
        avg = avgTemp.reading(distance);             // Lit une nouvelle valeur de distance et renvoi la moyenne

    */
  } else {
    avg = avgTemp.reading(distance_mm);             // Lit une nouvelle valeur de distance et renvoi la moyenne
    i = 0; // Réinitialisation de la variable si les mesures ne sont plus aberrantes (implique X mesures aberrantes d'afillées)
  }

  //distance_mm = distanceNC;

  /* Affiche les résultats en mm, cm et m */
  //Serial.print(F("Distance: "));
  Serial.print(distance_mm);
  Serial.print(F(", "));
  //Serial.print(distance_mm / 10.0, 2);
  //Serial.print(F("cm, "));
  //Serial.print(distance_mm / 1000.0, 2);
  //Serial.println(F("m)"));
  Serial.print(F(" , Distance Moyenne 8 NON corrigée: "));
  Serial.print(avg_sans_correction);
  Serial.print(F(" , Distance Moyenne 8 corrigée: "));
  Serial.print(avg);

  Serial.print(F(", Ecartype : "));
  Serial.println(ecart_type);


  //Serial.println();

  delay(250);
}
