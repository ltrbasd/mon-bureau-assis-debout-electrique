#ifndef Horizontalisation_HPP
#define Horizontalisation_HPP

// il faut bien penser à modifier l'axe qui est mesuré en fonction de la manière dont est placé l'inclinomètre sur le boitier
int LectureInclinometre () {
  // Lecture de l'inclinomètre pour voir l'inclinaison actuelle du plateau et l'arrêter lorsqu'il est horizontal
  if (inclin.available())
  {
    inclin.read(); // Lire la sortie du capteur d'inclinaison

    int Val = AngleMoy.reading(inclin.z);
    //    Serial.print(F("Angle instantané du plateau : "));
    //    Serial.println(Val2);
    //int Val = Val2 * 100;
    MoyA = Val + ConstCorrectionAngle;
    //    Serial.print(F("Angle moyen du plateau : "));
    //    Serial.println(MoyA);

    Serial.print(F("Angle instantané/ corrigé du plateau : "));
    Serial.print(Val);
    Serial.print(F(" , "));
    Serial.println(MoyA);

    delay(30);
  }
  return (MoyA);
}


int Horizontalisation () {

  // Lecvture de la hauteur du plateau pour faire une horizontalisation vers le haut ou vers le bas
  int DistMoy = FaireMesureUltraSon();
  //Serial.print(F("Distance H : "));
  //Serial.println(DistMoy);

  MoyA = LectureInclinometre ();

  if ( MoyA > 15 ) {
    Serial.println(F("  Plateau incliné à GAUCHE."));
    vitesse = 230;
    if (DistMoy > 850) { // Si le plateau est en montée auto, faire une horizontalisation vers le bas
      // Mettre le vérin droite dans le sens descente
      Serial.println(F("Droite descente"));
      digitalWrite(BorcheSensV2a, LOW);
      digitalWrite(BorcheSensV2b, HIGH);
    } else {
      //Mettre le vérin gauche dans le sens montée
      digitalWrite(BorcheSensV1a, HIGH);
      digitalWrite(BorcheSensV1b, LOW);
    }
    ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse normale
  }
  if ( MoyA < -15 ) {
    Serial.println(F("  Plateau incliné à DROITE."));
    vitesse = 230;
    if (DistMoy > 850) { // Si le plateau est en montée auto, faire une horizontalisation vers le bas
      // Mettre le vérin gauche dans le sens descente
      digitalWrite(BorcheSensV1a, LOW);
      digitalWrite(BorcheSensV1b, HIGH);
    } else {
      //Mettre le vérin droite dans le sens montée
      digitalWrite(BorcheSensV2a, HIGH);
      digitalWrite(BorcheSensV2b, LOW);
    }
    ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse normale
  }
  if (( MoyA > 0 ) && (MoyA < 15 )) {
    Serial.println(F("  Plateau faiblement incliné à GAUCHE."));
    vitesse = 130;
    if (DistMoy > 850) { // Si le plateau est en montée auto, faire une horizontalisation vers le bas
      // Mettre le vérin droite dans le sens descente
      digitalWrite(BorcheSensV2a, LOW);
      digitalWrite(BorcheSensV2b, HIGH);
    } else {
      digitalWrite(BorcheSensV1a, HIGH);
      digitalWrite(BorcheSensV1b, LOW);
    }
    ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse faible en approchant de l'horizontal
  }

  if (( MoyA < 0 )  && (MoyA > -15 )) {
    Serial.println(F("  Plateau faiblement incliné à DROITE."));
    vitesse = 140;
    if (DistMoy > 850) { // Si le plateau est en montée auto, faire une horizontalisation vers le bas
      // Mettre le vérin gauche dans le sens descente
      digitalWrite(BorcheSensV1a, LOW);
      digitalWrite(BorcheSensV1b, HIGH);
    } else {
      digitalWrite(BorcheSensV2a, HIGH);
      digitalWrite(BorcheSensV2b, LOW);
    }
    ActiverVerins (vitesse,vitesse); // Relancer la commande avec la vitesse faible en approchant de l'horizontal
  }

  if ((MoyA == -2) || (MoyA == -1) || (MoyA == 0) || (MoyA == 1) || (MoyA == 2)) {
    Serial.println(F("  Plateau HORIZONTAL."));
    StopperVerins (); // Stopper les vérins si le plateau est horizontal
    HorizontalisationenCours = 0;

  }
}

#endif
